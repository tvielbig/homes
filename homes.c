#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// DONE: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
home **readhomes(char *filename) 
{
    FILE *f = fopen(filename, "r");
    if(!f) printf("can't open %s for reading\n", filename);

    char addr[10000];
    int area,zip,price;   
    int count = 0;
    while(fscanf(f, " %i,%[^,],%i,%i", &zip, addr, &price, &area) != EOF)
    {
        count++;
    }
    
    
    count++; //adding a space for null position
    
    struct home **houses = malloc(sizeof(struct home)*count);
    count=0;
    rewind(f);
    while(fscanf(f, " %i,%[^,],%i,%i", &zip, addr, &price, &area) != EOF)
    {    
        struct home *h = (struct home *)malloc(sizeof(struct home));
        h->zip = zip;
        h->addr = malloc ( strlen ( addr ) + 1 );
        strcpy( h->addr, addr );
        h->price = price;
        h->area = area;
        houses[count] = h;
        count++;
    }
    houses[count+1] = NULL;
    count = 0;
    /*TEST PRINT
    while(houses[count] != NULL)
    {
        printf("%s\n", houses[count]->addr);
        count++;
    }
    */
    return houses;
}

int get_choice() 
{
    int c;
    scanf(" %d", &c);
    return c;
}

int numhomes(home **houses) {
    int index = 0;
    int count = 0;
    while(houses[index++] != NULL) count++;
    return count;
}

int numhomes_zip(home **houses, int zip) 
{
    int count = 0;
    int index = 0;
    while(houses[index] != NULL)
    {
        if(houses[index]->zip == zip) count++;
        index++;
    }
    return count;
}

void numhomes_price(home **houses, int price) 
{
    int count = 0;
    int index = 0;
    while(houses[index] != NULL)
    {
        int diff = (houses[index]->price) - price;
        if(diff > (-10000) && diff < 10000) 
        {
            printf("$%d      %s\n", houses[index]->price, houses[index]->addr);
            count++;
        }
        index++;
    }
    printf("%d homes in your price range\n", count);
}
int main(int argc, char *argv[])
{
    // DONe: Use readhomes to get the array of structs
    struct home **houses = readhomes("listings.csv");
    
    
    // At this point, you should have the data structure built
    // DONE: How many homes are there? Write a function, call it,
    // and print the answer.
    printf("The number of homes: %d\n", numhomes(houses));
    
    // DONE: Prompt the user to type in a zip code.
    printf("Enter a 5 digit zip code: ");
    int z;
    scanf(" %d", &z);
    
    // At this point, the user's zip code should be in a variable
    // DONE: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    printf("The number of homes in that zipcode: %d\n", numhomes_zip(houses, z));

    // DONE: Prompt the user to type in a price.
    printf("Enter your ideal price, ommitting any commas: $");
    scanf(" %d", &z);

    // At this point, the user's price should be in a variable
    // DONE: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    numhomes_price(houses, z);
}   
